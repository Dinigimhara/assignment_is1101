#include <stdio.h>
#define performanceCost 500
#define attendeeCost 3

//function declaration
int attendees(int price);
int income(int price, int attendees);
int cost(int attendees);
int profit(int income, int cost);


int main()
{
  int x, y, c ; //x=ticket price, y=profit, c=attendees

	for(x=5; x<=50; x +=5){
		c=attendees(x);
        y=profit(income(x,c), cost(c));
		printf("Rs. %d\t\tRs. %d \n" ,x,y);

	}
        printf("\nHighest profit is Rs.1260 \n");
		printf("Ticket price to get the highest profit is Rs.25 \n");
		return 0;
}


//function definition

int attendees(int price)
{
    return 120-(price - 15)/5*20;
}

int income(int price,int attendees)
{
    return price * attendees;
}

int cost(int attendees)
{
    return performanceCost+(attendeeCost * attendees);
}

int profit(int income, int cost)
{
    return income - cost;
}



