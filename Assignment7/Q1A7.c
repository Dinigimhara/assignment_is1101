#include <stdio.h>
#include <string.h>

void sentenceReverse(char s[])
{
    int length = strlen(s);

    for (int i = length - 1; i >= 0; i--) {
        if (s[i] == ' ') {
            s[i] = '\0';  // NULL Character replace space characters for next iteration.
            printf("%s ", &(s[i]) + 1);
        }
    }

    // printing the last word
    printf("%s", s);
}

int main()
{
    char s[1000];
    printf("Enter the sentence: \n");
    gets(s);
    sentenceReverse(s);
    return 0;
}


