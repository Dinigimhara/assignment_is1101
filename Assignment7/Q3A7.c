#include<stdio.h>
int main()
{
    int ROW, COL;
    printf("Enter number of rows and columns: \n");
    scanf("%d %d", &ROW, &COL);

    int i, j, arr1[ROW][COL], arr2[ROW][COL], arr3[ROW][COL];

    printf("Enter first matrix: \n");

    for(i = 0; i < ROW; i++)
    {
        for(j = 0; j < COL; j++)
        {
            scanf("%d", &arr1[i][j]);
        }
    }

    printf("Enter second matrix: \n");

    for(i = 0; i < ROW; i++)
    {
        for(j = 0; j < COL; j++)
        {
            scanf("%d", &arr2[i][j]);
        }
    }

    printf("arr1 + arr2 = \n");

    // add two matrices
    for(i = 0; i < ROW; i++)
    {
        for(j = 0; j < COL; j++)
        {
            printf("%d ", arr1[i][j] + arr2[i][j]);
        }
        printf("\n");
    }

    printf("\n arr1 * arr2 = ");

    // multiply two matrices
    for(i = 0; i < ROW; i++)
    {
        for(j = 0; j < COL; j++)
        {
            arr3[i][j] = 0;

            for(int k = 0; k < COL; k++)
            {
                arr3[i][j] += arr1[i][k] * arr2[k][j];
            }
        }

        printf("\n");
    }

    // print the result
    for(i = 0; i < ROW; i++)
    {
        for(j = 0; j < COL; j++)
        {
            printf("%d ", arr3[i][j]);
        }
        printf("\n");
    }


    return 0;
}
