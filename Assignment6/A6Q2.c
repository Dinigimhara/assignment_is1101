#include<stdio.h>

int  fibonacciSeq(int n)
{
   if ( n == 0 || n==1 )
      return n;

   else
      return (  fibonacciSeq(n-1) +  fibonacciSeq(n-2) );

}


int main()
{
   int n;

    printf("Enter terms: ");
    scanf("%d", &n);

    for(int i = 0; i <= n; i++)
    {
        printf("%d ", fibonacciSeq(i));
        printf("\n");
    }

   return 0;
}

